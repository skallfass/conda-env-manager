# -*- coding: utf-8 -*-


from setuptools import find_packages
from setuptools import setup
import subprocess


setup(name='cenv',
      version=(subprocess.check_output(['git', 'describe', '--tag'])
               .strip()
               .decode('ascii')
               .replace('-', '_')),
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False)
