# cenv

## Installation
Create new conda-environment with this package as only requirement.

```
conda create -n cenv_tool cenv
```

Then create an alias in your .zshrc / .bashrc like the following

```
alias cenv="/shared/conda/envs/cenv_tool/bin/cenv"
```

Now you can use cenv from terminal with only typing **cenv**.
For example:

```
cenv --help
```

``cenv`` can be run in default-mode or in auto-mode.
In default-mode ``cenv`` will only display the commands required to create
/ update the conda-environment of the current project.
No modifications are made by ``cenv`` itself.

In auto-mode all commands are executed by ``cenv``. If removing of the
existing environment is required to update the environment the user is asked
for confirmation.
The auto-mode is triggered by the flag ``-a``.

cenv can be run with the following flags:
* ``-a``: triggers the auto-mode
* ``-i``: show informations about current project and conda itself
* ``-d``: show this README with vmd and exit
* ``-v``: show cenv-version and exit
* ``--search <package_name>``: Searchs for avaiability of package in
  conda-channels. If package could not be found in conda-channels, searches on
  pypi and shows the commands to convert the package from a pypi-package to
  a conda-package. This can be used as a replacement for the ``conda
  search``-command.

> **ATTENTION:**
> ``cenv`` can only update the environment if it is not activated.
> So ensure the environment to be deactivated before running ``cenv``.

``cenv`` requires a ``meta.yaml`` file in projects subfolder
``conda-build/meta.yaml``.

In the ``extra``-section the name of the conda-environment to create / update
is defined.

``cenv`` uses ``/opt/conda`` as default installation path of conda and
``/shared/conda/envs`` as installation path for conda-environments.
If you want to change this you have to create a ``.cenv.yml`` file in your
``home``-folder with the following content (with your path-definitions):

```yaml
conda_folder: /opt/conda
env_folder: /shared/conda/envs
```

## Example for a valid meta.yaml used by cenv

```
{% set data = load_setup_py_data() %}

package:
    name: "example_package"
    version: {{ data.get("version") }}

source:
    path: ..

build:
    build: {{ environ.get('GIT_DESCRIBE_NUMBER', 0) }}
    preserve_egg_dir: True
    script: python setup.py install

requirements:
    build:
      - python 3.6.8
      - setuptools
    run:
      - python 3.6.8
      - attrs >=18.2
      - jinja2 >=2.10
      - ruamel.yaml >=0.15.23
      - six >=1.12.0
      - yaml >=0.1.7
      - marshmallow >=3.0.0rc1*

test:
    imports:
        - example_package

extra:
    env_name: example
```

> **ATTENTION:**
>
> Ensure to add a version to each package in ``requirements-run``-section.
> Otherwise cenv will not update the projects conda-environment not defining
> a version for a package is defined to be an invalid ``meta.yaml``-definiton.
