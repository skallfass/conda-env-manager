"""
Description
-----------
cenv is a tool to handle conda-environment creation and update from the
dependency-definition inside the meta.yaml file.

As default conda has two files for dependency management:
* the environment.yml
* and the meta.yaml

In the environment.yml the environment-definition is stored.
In the meta.yaml the required informations to build a conda-package are
stored. This means redundant informations.

It can be run in the default mode and automode.
The default-mode shows all relevant informations about the current project
and the conda-commands to create / update the environment.

The automode is triggered by the flag -a. In this mode the same informations
are displayed like in the default-mode but the creation / update process
itself is run automatically. If the environment already exists, the user
is asked for confirmation to recreate the environment.

cenv collects the dependency-informations and all project-specific settings
from the meta.yaml-file.

Information
-----------
* authors:
    * Simon Kallfass
* python:
    * 3.6
"""

import argparse
import os
from pathlib import Path
from pkg_resources import get_distribution
import subprocess
import sys
from typing import Type
from typing import TypeVar
from typing import NoReturn

import attr
from ruamel.yaml import YAML

from cenv.utils import available_envs
from cenv.utils import beautify
from cenv.utils import print_bold
from cenv.utils import print_cyan
from cenv.utils import print_red
from cenv.utils import stat
from cenv.utils import GREEN
from cenv.utils import NCOLOR
from cenv.utils import print_cmd
from cenv.utils import run_in_bash
from cenv.utils import read_config
from cenv.utils import read_meta_yaml
from cenv.rules import RULES


try:
    __version__ = get_distribution('cenv').version
except:
    __version__ = ''


CONFIG = read_config()
DEFAULT_CONDA_FOLDER = Path(CONFIG['conda_folder'])
DEFAULT_ENV_FOLDER = Path(CONFIG['env_folder'])


TEnvCmds = TypeVar('TEnvCmds', bound='EnvironmentCommands')
TProject = TypeVar('TProject', bound='Project')


@attr.s(slots=True,
        frozen=True,
        auto_attribs=True)
class EnvironmentCommands:
    """
    Contains the relevant conda commands for a specific project.

    Should be build using the build-classmethod.
    """
    remove: str
    activate: str
    export: str
    create: str
    clone: str
    restore: str
    clean: str
    search: str
    search_pip: str

    @classmethod
    def build(cls: Type[TEnvCmds],
              conda_bin: str,
              name: str,
              dependencies: list,
              auto: bool,
              conda_bin_auto: Path) -> TEnvCmds:
        """
        Depending on the auto-parameter the conda-commands for the project are
        created.
        In auto-mode these commands are built to be used by the
        subprocess.check_output function (this includes the "-y" flag of
        the conda commands).
        In default-mode (auto=False) the commands are created to be used by
        the user after printed out.

        Parameters
        ----------
        cls
            The EnvironmentCommands-Class
        conda_bin
            The path to the conda-executable (in case of manual mode this
            is only "conda").
        name
            The name of the conda-environment
        dependencies
            The dependencies of the conda-environment
        auto
            Flag if conda-commands should be created for auto-mode

        Returns
        -------
        cmds
            Instance of EnvironmentCommands-Class
        """
        auto_cmd = lambda x: f'{x} -y'

        for idx, dep in enumerate(dependencies):
            if not '=' in dep:
                dep = dep.replace(' ', '=')
            dependencies[idx] = dep.replace(' ', '')

        search = '{conda} search '.format(conda=conda_bin_auto)
        search_pip = '{pip} search '.format(pip=conda_bin_auto.with_name('pip'))
        if auto:
            pkgs = ' '.join([f'"{_}"' for _ in dependencies])
            remove = auto_cmd(RULES.conda_cmds.remove)
            activate = auto_cmd(RULES.conda_cmds.activate)
            export = RULES.conda_cmds.export
            create = auto_cmd(RULES.conda_cmds.create)
            clone = auto_cmd(RULES.conda_cmds.clone)
            restore = auto_cmd(RULES.conda_cmds.restore)
            clean = auto_cmd(RULES.conda_cmds.clean)
        else:
            pkgs = ' \\\n        '.join([f'"{_}"' for _ in dependencies])
            remove = RULES.conda_cmds.remove
            activate = RULES.conda_cmds.activate
            export = RULES.conda_cmds.export
            create = RULES.conda_cmds.create
            clone = RULES.conda_cmds.clone
            restore = RULES.conda_cmds.restore
            clean = RULES.conda_cmds.clean

        cmds = cls(remove=remove.format(conda=conda_bin, name=name),
                   activate=activate.format(conda=conda_bin, name=name),
                   export=export.format(conda=conda_bin, name=name),
                   create=create.format(conda=conda_bin, name=name, pkgs=pkgs),
                   clone=clone.format(conda=conda_bin, name=name),
                   restore=restore.format(conda=conda_bin, name=name),
                   clean=clean.format(conda=conda_bin, name=name),
                   search=search,
                   search_pip=search_pip)
        return cmds


@attr.s(slots=True,
        frozen=True,
        auto_attribs=True)
class Project:
    """
    Representing a python-project using conda-environments.

    Containing methods to display informations to current project and methods
    to update the projects conda-environment from the settings defined in the
    projects meta.yaml file.
    """
    conda_folder: Path = attr.ib(converter=Path)
    env_folder: Path = attr.ib(converter=Path)
    env_name: str
    auto: bool
    info: bool
    docu: bool
    search: str
    dependencies: dict
    yes: bool
    is_env: bool = attr.ib()
    is_git: bool = attr.ib(default=(Path.cwd() / RULES.git_folder).exists())
    cmds: EnvironmentCommands = attr.ib()

    @is_env.default
    def set_is_env(self) -> bool:
        """
        Sets default value for ``self.is_env`` by checking if the name of the
        projects conda-environment is already inside condas environment-folder.

        Returns
        -------
        is_env
            check if the projects environment already exists
        """
        is_env = self.env_name in available_envs(self.conda_folder)
        return is_env

    @cmds.default
    def set_cmds(self) -> EnvironmentCommands:
        """
        Sets default value for the conda commands in ``self.cmds`` using the
        ``EnvironmentCommands``-class.

        Returns
        -------
        cmds
            an instance of the EnvironmentCommands-class
        """
        conda_bin = RULES.conda_cmds.conda_bin(self.conda_folder)
        cmds = EnvironmentCommands.build(
            conda_bin=conda_bin if self.auto else 'conda',
            name=self.env_name,
            dependencies=self.dependencies['list'],
            auto=self.auto,
            conda_bin_auto=conda_bin)
        return cmds

    def run(self):
        """
        Calls the method to display informations about current project.
        If auto-mode is active, the creation- / update-process is run by
        calling the update-method. Else the commands to run are displayed
        by calling the show_update_commands-method.
        """
        if self.docu:
            os.system(f'{self.env_folder / "cenv_tool/bin/vmd"} {(Path(__file__).parent / "README.md").absolute()}')
            return
        if self.search:
            self.search_package()
            return
        if self.info:
            self.show_info()
        if self.auto:
            self.update()
        else:
            self.show_update_commands()

    @classmethod
    def from_meta_yaml(cls: Type[TProject],
                       auto: bool,
                       info: bool,
                       docu: bool,
                       search: str,
                       yes: bool) -> TProject:
        """
        Classmethod creating a Project-class from content of the meta.yaml
        file of the current project.

        Parameters
        ----------
        cls
            Project-class

        Returns
        -------
        Instance of Project-class
        """
        try:
            meta_yaml, dependencies = read_meta_yaml(Path.cwd())
            settings = meta_yaml['extra']
        except FileNotFoundError:
            if any([docu, search]) and not auto:
                settings = dict(env_name='')
                dependencies = dict(list=[])

            else:
                print_red('project has no meta.yaml!')
                sys.exit(1)

        settings['conda_folder'] = DEFAULT_CONDA_FOLDER
        settings['env_folder'] = DEFAULT_ENV_FOLDER
        settings['auto'] = auto
        settings['info'] = info
        settings['docu'] = docu
        settings['search'] = search
        settings['dependencies'] = dependencies
        settings['yes'] = yes
        if settings.get('dev_requirements'):
            settings.pop('dev_requirements')
        return cls(**settings)

    def show_info(self) -> NoReturn:
        """
        Prints all relevant informations for the project.
        """
        beautify('Available environments', print_bold)
        for env in available_envs(self.conda_folder):
            print(f'    - {env}')
        print()

        beautify('Project info', print_bold)
        stat('name', self.env_name)
        stat('git', self.is_git)
        stat('env exists', self.is_env)

        print()
        beautify('Dependencies', print_bold)
        for pkg in self.dependencies['packages']:
            print_bold(f'    {pkg["name"]:>25s}  {pkg["version"]}')
        print()

        if self.is_env:
            beautify(f'Packages installed in {self.env_name}', print_bold)
            print(run_in_bash(f'{self.conda_folder / "bin/conda"} list -n {self.env_name}'))
            print()

    def show_update_commands(self) -> NoReturn:
        """
        Prints the conda commands to execute for an environment-update to be
        executed manually by the user.
        """
        beautify(f'Commands to update {self.env_name}', print_bold)
        print_bold('Note: You can also run:')
        print('    cenv -a')
        print_bold('to run the following commands in auto-mode.')
        print()

        if self.is_env:
            print_red('ATTENTION:')
            print_red('The required env already exists. Remove it '
                      'before recreation.')
            print()
            print_cmd(cmd=self.cmds.clone, text='Create a backup using:')
            print_cmd(cmd=self.cmds.remove, text='Remove env with:')

        print_cmd(cmd=self.cmds.create, text='Create env with:')

        if self.is_env:
            print_cmd(cmd=self.cmds.restore,
                      text=('If any failure occured during creation reset '
                            'from backup:'))
            print_cmd(cmd=self.cmds.clean,
                      text='Now remove the backup:')

        print_cmd(cmd=self.cmds.activate, text='Activate env using:')
        print_cmd(cmd=self.cmds.export,
                  text='Export env to environment.yml:')

    def update(self) -> NoReturn:
        """
        Creates / recreates the conda-environment of the current project.

        If the conda-environment already exists the user is aked for
        confirmation to continue. Then the environment will be cloned as a
        backup and the original environment will be removed. Now the new
        conda-environment will be created. If a backup was created it is
        removed afterwards. If any errors occur during creation of the new
        environment the old environment will be recreated from backup and
        then the backup will be removed. Finally the environment-definition
        of the created environment is exported to an environment.yml file.
        """
        print()
        if self.is_env:
            print_bold(f'Updating env [{GREEN}AUTOMODE{NCOLOR}]')
        else:
            print_bold(f'Creating env [{GREEN}AUTOMODE{NCOLOR}]')
        cloned = False
        print()
        if self.is_env:
            beautify('ATTENTION: this will remove the existing '
                     f'env {self.env_name} before recreation. ',
                     print_red)
            if self.yes:
                check = True
            else:
                check = input('Continue? [y/n]: ') == 'y'
            if check:
                backup_name = f'{self.env_name}_backup'
                if backup_name in available_envs(self.conda_folder):
                    print_cyan('┣━━ Removing old backup ...')
                    run_in_bash(cmd=self.cmds.clean)
                print_cyan('┣━━ Cloning existing env as backup ...')
                run_in_bash(cmd=self.cmds.clone)
                cloned = True
                try:
                    print_cyan('┣━━ Removing existing env ...')
                    run_in_bash(cmd=self.cmds.remove)
                except subprocess.CalledProcessError:
                    run_in_bash(cmd=self.cmds.clean)
                    print_red('Could not remove environment because it is '
                              'activated! Please deactivate it first.')
                    sys.exit(1)
            else:
                print_red('┗━━ ... Exited due userinput!')
                sys.exit(1)
        print_cyan('┣━━ Creating env ...')
        try:
            run_in_bash(cmd=self.cmds.create)
        except:
            print_red('┣━━ Error during creation!')
            if self.is_env and cloned:
                print_cyan('┣━━ Recreating env from backup ...')
                run_in_bash(cmd=self.cmds.restore)
                run_in_bash(cmd=self.cmds.clean)
            print_red('┗━━ Exit')
            raise
        if cloned:
            print_cyan('┣━━ Removing backup ...')
            run_in_bash(cmd=self.cmds.clean)
        #print_cyan('┗━━ Exporting env to environment.yml ...')
        #run_in_bash(cmd=self.cmds.export)
        print()

    def search_package(self) -> NoReturn:
        beautify(f'Search results for availability of conda-package {self.search}', print_bold)
        try:
            print(run_in_bash(f'{self.cmds.search} {self.search}'))
        except subprocess.CalledProcessError:
            try:
                print(run_in_bash(f'{self.cmds.search_pip} {self.search}'))
                print_red(f'{self.search} not available in current channels.')
                print_red('The printed packages are results '
                          'of the availability of this package in pip.')
                print_red('You\'ll have to convert the package to a '
                          'conda-package yourself.')
                print_red('Create the required skeleton with:')
                print(f'    conda skeleton pypi {self.search} '
                      '--python=[DESIRED_PYTHON_VERSION]')
                print_red('Then build the package using:')
                print(f'    conda build {self.search} '
                      '--python=[DESIRED_PYTHON_VERSION]')
            except subprocess.CalledProcessError:
                print_red(f'Package {self.search} neither available as '
                          'conda-package, nor pypi-package.')


def build_arguments() -> argparse.Namespace:
    """
    Creates arguments for the cenv-tool.

    Returns
    -------
    args
        The parsed arguments
    """
    parser = argparse.ArgumentParser(
        description=('cenv is a tool to handle conda-environment creation '
                     'and update from the dependency-definition inside '
                     'the meta.yaml file. It shows all relevant commands '
                     'to update / create the environment yourself.'
                     'In auto-mode (flag -a) these commands are executed '
                     'by cenv and you do not have to run these commands '
                     'yourself. cenv can also be used to display all '
                     'relevant informations to a project. This can be '
                     'shown with the -i flag.'))
    parser.add_argument(
        '-a',
        '--automode',
        action='store_true',
        default=False,
        help=('run cenv in automode. This creates the projects '
              'conda-environment from the dependency informations inside the '
              'meta.yaml-file. If the conda-environment already exists, a '
              'clone of the environment is created as backup. Then the '
              'existing conda-environment will be removed and recreated. '
              'If any error occures during recreation, the conda-environment '
              'will be restored from the backup environment. Finally the '
              'environment-definition will be saved to an environment.yml '
              'file to be used on other machines / machines without cenv to '
              'create the projects conda-environment.'))
    parser.add_argument(
        '-i',
        '--info',
        action='store_true',
        default=False,
        help=('Show details about current project, including informations '
              'about conda itself, currently installed conda-environments, '
              'major dependencies of conda-environment of current project, '
              'detailed list of all installed packages of environment '
              'of current project, etc.'))
    parser.add_argument(
        '-d',
        '--documentation',
        action='store_true',
        help=('Show detailed manual for usage of cenv using the tool vmd.'))
    parser.add_argument(
        '-y',
        '--yes',
        action='store_true',
        help=('Do not ask for confirmation on auto-update.'))
    parser.add_argument(
        '-s',
        '--search',
        type=str,
        default='',
        help=('Wrapper around condas search command to search for '
              'availability of the passed package-name.'))
    parser.add_argument(
        '-v',
        '--version',
        action='store_true',
        default=False,
        help='Show current version of cenv and exit.')
    args = parser.parse_args()
    return args


def main() -> NoReturn:
    """
    The main-function collecting the required arguments and initializing the
    Project-instance and calling its run-method.
    """
    args = build_arguments()
    if args.version:
        print(__version__)
        sys.exit(0)
    print()
    project = Project.from_meta_yaml(auto=args.automode,
                                     info=args.info,
                                     docu=args.documentation,
                                     search=args.search,
                                     yes=args.yes)
    project.run()
    print()


if __name__ == '__main__':
    main()
