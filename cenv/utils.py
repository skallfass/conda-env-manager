"""
Description
-----------
Utils like colored printing, command-execution in bash, reading of
informations from the meta.yaml-file, etc. used by cenv.

Information
-----------
* authors:
    * Simon Kallfass
* python:
    * 3.6
"""


import os
from pathlib import Path
import subprocess
import sys
from typing import Union
from typing import List
from typing import NoReturn

import jinja2
from marshmallow import fields
from marshmallow import Schema
from marshmallow import validate
from marshmallow import ValidationError
from ruamel.yaml import YAML
import six


CYAN = '\033[0;94m'
GREEN = '\033[1;92m'
RED = '\033[1;91m'
NCOLOR = '\033[0m'
BOLD = '\033[1;37m'


def print_red(text: str) -> NoReturn:
    """
    wrapper function to print the passed text in red on terminal.

    Paramaters
    ----------
    text
        The text to print in red on terminal
    """
    print(f'{RED}{text}{NCOLOR}')


def print_green(text: str) -> NoReturn:
    """
    wrapper function to print the passed text in green on terminal.

    Paramaters
    ----------
    text
        The text to print in green on terminal
    """
    print(f'{GREEN}{text}{NCOLOR}')


def print_bold(text: str) -> NoReturn:
    """
    wrapper function to print the passed text in bold on terminal.

    Paramaters
    ----------
    text
        The text to print in bold on terminal
    """
    print(f'{BOLD}{text}{NCOLOR}')


def print_cyan(text: str) -> NoReturn:
    """
    wrapper function to print the passed text in cyan on terminal.

    Paramaters
    ----------
    text
        The text to print in cyan on terminal
    """
    print(f'{CYAN}{text}{NCOLOR}')


def available_envs(conda_folder: Path) -> List[str]:
    """
    Collects the names of the conda-environments currently installed.

    Returns
    -------
    envs
        List of currently installed conda-environments
    """
    #envs = [_.stem for _ in env_folder.iterdir() if _.is_dir()]
    envs = run_in_bash(str(conda_folder.absolute()) + '/bin/conda env list | awk \'{ if( !($1=="#") ) print $1 }\'').split('\n')
    return envs


def stat(content: str, status: Union[bool, str, int, float]) -> NoReturn:
    if status:
        template = f' {content:>20s} ┃  {GREEN}{status}{NCOLOR}'
    else:
        template = f' {content:>20s} ┃  {RED}{status}{NCOLOR}'
    print(template.format(content=content, status=status))


def beautify(text: str,
             print_function: Union[print_bold,
                                   print_green,
                                   print_cyan,
                                   print]) -> NoReturn:
    #print_function(f'┏{"━" * (len(text) + 2)}┓')
    print_function(f'┃ {text}')
    print_function(f'┗{"━" * (len(text) + 2)}')


def print_cmd(cmd: str, text: str) -> NoReturn:
    print_cyan(text)
    print(f'    {cmd}')
    print()


def run_in_bash(cmd: str) -> str:
    """
    Runs the passed cmd inside bash using the subprocess.check_output-function.
    """
    return subprocess.check_output(
        [cmd],
        shell=True,
        stderr=subprocess.STDOUT).strip().decode('ascii')


class SNPackage(Schema):
    name = fields.String(strict=True,
                         required=True)
    version = fields.String(strict=True,
                            required=True)


class SNSource(Schema):
    path = fields.String(strict=True,
                         required=True)


class SNBuild(Schema):
    """
    Schema for the build-section inside a meta.yaml file, to check this section
    of a given meta.yaml file in cenv, to be valid.
    The build-section requires to define the build-number, if the egg-dir
    should be preserved, the script to run on installation and if any
    entrypoints are defined for the package.
    """
    build = fields.String(strict=True,
                          required=True)
    preserve_egg_dir = fields.String(strict=True,
                                     required=True,
                                     validate=validate.OneOf(['True', 'False']))
    script = fields.String(strict=True,
                           required=True)
    entry_points = fields.List(fields.String(strict=True, required=False),
                               strict=True,
                               required=False)


class SNRequirements(Schema):
    """
    Schema for the requirements-section inside a meta.yaml file, to check this
    section of a given meta.yaml file in cenv, to be valid.
    The underlaying build- and run-sections have to be valid!
    """
    build = fields.List(fields.String(strict=True,
                                      required=True),
                        strict=True,
                        required=True)
    run = fields.List(fields.String(strict=True,
                                    required=True,
                                    validate=lambda x: '=' in x if not 'python ' in x else True,
                                    error_messages=dict(validator_failed='Version must be specified')),
                      strict=True,
                      required=True)


class SNTest(Schema):
    """
    Schema for the test-section inside a meta.yaml file, to check this section
    of a given meta.yaml file in cenv, to be valid.
    """
    imports = fields.List(fields.String(strict=True,
                                        required=False),
                          strict=True,
                          required=False)
    commands = fields.List(fields.String(strict=True,
                                         required=False),
                           strict=True,
                           required=False)


class SNExtra(Schema):
    """
    Schema for the extra-section inside a meta.yaml file, to check this
    section of a given meta.yaml file in cenv, to be valid.
    The extra-section has to contains the informations where to find the
    conda-folder, the name of the conda-environment to use for the current
    project and the cenv-version used when the meta.yaml file was created.
    """
    env_name = fields.String(strict=True,
                             required=True,
                             validate=lambda x: ' ' not in x)

    dev_requirements = fields.List(
        fields.String(strict=True,
                      required=True,
                      validate=lambda x: '=' in x,
                      error_messages=dict(validator_failed='Version must be specified')),
        strict=True,
        required=False)


class SMetaYaml(Schema):
    """
    Schema for a meta.yaml file to be used for cenv.
    Ensures the meta.yaml to load contains the relevant informations about
    the package, source, build, requirements and extra.
    The test-section is optional.
    """
    package = fields.Nested(SNPackage,
                            strict=True,
                            required=True)
    source = fields.Nested(SNSource,
                           strict=True,
                           required=True)
    build = fields.Nested(SNBuild,
                          strict=True,
                          required=True)
    requirements = fields.Nested(SNRequirements,
                                 strict=True,
                                 required=True)
    test = fields.Nested(SNTest,
                         strict=True,
                         required=False)
    extra = fields.Nested(SNExtra,
                          strict=True,
                          required=True)


class NullUndefined(jinja2.Undefined):
    """
    Class required to handle jinja2-variables with undefined content
    inside the meta.yaml
    """
    def __unicode__(self):
        return six.text_type(self._undefined_name)

    def __getattr__(self, attribute_name):
        return six.text_type(f'{self}.{attribute_name}')

    def __getitem__(self, attribute_name):
        return f'{self}["{attribute_name}"]'


class StrDict(dict):
    """
    Class required to handle jinja2-variables inside the meta.yaml
    """
    def __getitem__(self, key: str, default: str = '') -> str:
        return self[key] if key in self else default


def read_meta_yaml(path: Path) -> dict:
    """
    Reads the meta.yaml file from relativ path conda-build/meta.yaml inside
    the current path, validates the meta.yaml using the marshmallow-schema,
    extracts the dependency-informations and the project-settings and returns
    these informations.

    Parameters
    ----------
    path
        The current working directory

    Returns
    -------
    project_info
        List containing the project-settings as a dict and the dependencies
        also as a dict
    """
    # load the meta.yaml-content
    myaml_content = (path / 'conda-build/meta.yaml').open().read()
    rendered_myaml = (jinja2.Environment(undefined=NullUndefined)
                      .from_string(myaml_content)
                      .render(**dict(os=os,
                                     environ=StrDict(),
                                     load_setup_py_data=StrDict)))
    loaded_myaml = YAML(typ='base').load(rendered_myaml)

    # validate the content of loaded meta.yaml
    try:
        dumped = SMetaYaml().dumps(loaded_myaml)
        settings = SMetaYaml().loads(dumped)
    except ValidationError as err:
        print_red('meta.yaml file is not valid!')
        print_red(f'ValidationError in {err.args[0]}')
        sys.exit(1)

    # extract the dependencies defined the the requirements-run-section
    deps = settings['requirements']['run']
    pkgs_list = [dict(name=package, version=version)
                 for package, version in
                 [_.split() for _ in deps]]
    if settings['extra'].get('dev_requirements'):
        extra_pkgs = [dict(name=package, version=version)
                    for package, version in
                    [_.split() for _ in settings['extra']['dev_requirements']]]
        pkgs_list.extend(extra_pkgs)
        deps.extend(settings['extra']['dev_requirements'])

    dependencies = dict(packages=pkgs_list, list=deps)

    # combine the collected project-settings and the collected dependencies
    # to one output of this function
    project_info = settings, dependencies
    return project_info


def read_config():
    user_config_path = Path.home() / '.cenv.yml'
    default_config_path = Path(__file__).parent / '.cenv.yml'

    # Collect settings from config file .cenv.yml
    if user_config_path.exists():
        config = YAML(typ='safe').load(user_config_path.open().read())
    else:
        config = YAML(typ='safe').load(default_config_path.open().read())
    return config
